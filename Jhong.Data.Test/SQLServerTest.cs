﻿namespace Jhong.Data.Test
{
    using Jhong.Data.Core.Common;
    using Jhong.Data.SQLServer;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;

    [TestClass]
    public class SQLServerTest
    {
        private DBContext dbContext = new SQLServerContext("Data Source=.;Initial Catalog=test;Integrated Security=True");

        [TestMethod]
        public void Init()
        {
            this.dbContext
            .S("if exists(select TABLE_NAME from INFORMATION_SCHEMA.TABLES where TABLE_NAME='person') drop table person")
            .S("create table person(id int  not null primary key identity,name varchar(20) not null)")
            .S("insert into person (name) values ('xiaoming')")
            .S("insert into person (name) values ('xiaohong')")
            .Execute();
        }

        [TestMethod]
        public void Test01()
        {
            var res1 = this.dbContext.S("select top 1 * from person ").S("select top 1 * from person where id>1").QueryToDataTables();
            Assert.AreEqual(res1.Count(), 2);
            Assert.AreEqual(res1[0].Rows.Count, 1);
            Assert.AreEqual(res1[1].Rows.Count, 1);
            var dbparemeters = new DbParameter[1];
            dbparemeters[0] = new SqlParameter("@id", System.Data.SqlDbType.Int);
            dbparemeters[0].Value = 1;
            var res2 = this.dbContext.S("select top 1 * from person where id>@id", dbparemeters).QueryFirstOrDefault<Person>();
            Assert.IsNotNull(res2);
            var res3 = this.dbContext.S("select * from person ").QueryToEntitys<Person>();
            Assert.AreEqual(res3.Count(), 2);
        }

        [TestMethod]
        public void Test02()
        {
            var res1 = this.dbContext.T("select top 1 * from person where name=@0", "xiaoming").QueryToDataTables();
            Assert.IsNotNull(res1);
            var res2 = this.dbContext.T("select * from person where name=@0 and id>@1", "xiaoming", 10).QueryToEntitys<Person>();
            Assert.AreEqual(res2.Count(), 0);
        }

        [TestMethod]
        public void Test3()
        {
            var person = new Person() { Name = "test" };
            var res1 = this.dbContext.T("select count(*) from person where name=@0", person.Name).QueryFirstOrDefault<int>();
            Assert.AreEqual(res1, 0);
            this.dbContext.InsertEntity(person);
            res1 = this.dbContext.T("select count(*) from person where name=@0", person.Name).QueryFirstOrDefault<int>();
            Assert.AreEqual(res1, 1);
            person = this.dbContext.T("select * from person where name=@0", person.Name).QueryFirstOrDefault<Person>();
            person.Name = "abc";
            this.dbContext.UpdateEntity(person);
            res1 = this.dbContext.T("select count(*) from person where name=@0", person.Name).QueryFirstOrDefault<int>();
            Assert.AreEqual(res1, 1);
            this.dbContext.Delete(person);
            res1 = this.dbContext.T("select count(*) from person where name=@0", person.Name).QueryFirstOrDefault<int>();
            Assert.AreEqual(res1, 0);
        }

        [TestMethod]
        public void Dispose()
        {
            this.dbContext.Dispose();
        }
    }
}