﻿namespace Jhong.Data.Core.Common
{
    using System.Data;

    public interface IDBMethod
    {
        int[] Execute();

        DataTable[] QueryToDataTables();
    }
}