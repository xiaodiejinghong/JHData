﻿namespace Jhong.Data.Test
{
    using Jhong.Data.Core;
    using Jhong.Data.Core.Common;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using MySql.Data.MySqlClient;
    using MYSQL;
    using System;
    using System.Data.Common;
    using System.Linq;

    [TestClass]
    public class MySQLTest
    {
        private DBContext dbContext = new MYSQLContext(new MySqlConnection("Server=127.0.0.1;Database=test;Uid=root;Pwd=root;Port=3306;"));

        [TestMethod]
        public void Init()
        {
            //this.dbContext = new MYSQLContext(new MySqlConnection("Server=127.0.0.1;Database=test;Uid=root;Pwd=root;Port=3306;"));
        }

        [TestMethod]
        public void Query1()
        {
            var dts = this.dbContext.S("select * from person").QueryToDataTables();
            Assert.IsTrue(dts.Count() > 0);
            Assert.IsTrue(dts[0].Rows.Count > 0);
        }

        [TestMethod]
        public void Query2()
        {
            var dts = this.dbContext.S("select * from person")
                .S("select * from testtable")
                .QueryToDataTables();
            Assert.AreEqual(dts.Count(), 2);
            Assert.AreEqual(dts[1].Rows.Count, 1);
        }

        [TestMethod]
        public void Exec1()
        {
            var parameters = new DbParameter[] { new MySqlParameter("@name", MySqlDbType.VarChar) };
            parameters[0].Value = "test-test";
            var results = this.dbContext.S("insert into person(Name) values (@name)", parameters).Execute();
            Assert.AreEqual(results.Count(), 1);
            Assert.AreEqual(results[0], 1);
        }

        [TestMethod]
        public void Exec2()
        {
            var parameters = new DbParameter[] { new MySqlParameter("@name", MySqlDbType.VarChar) };
            parameters[0].Value = "test-test";
            var currCount = Convert.ToInt32(this.dbContext.S("select count(*) from person").QueryToDataTables()[0].Rows[0][0]);
            var results = this.dbContext
                .S("insert into person(Name) values (@name)", parameters)
                .S("insert into person(Name) values (@name)", parameters)
                .S("insert into person(Name) values (@name)", parameters)
                .Execute();
            Assert.AreEqual(results.Count(), 3);
            int now = Convert.ToInt32(this.dbContext.S("select count(*) from person").QueryToDataTables()[0].Rows[0][0]);
            Assert.AreEqual(currCount + 3, now);
            var delCount = Convert.ToInt32(this.dbContext.S("delete FROM person WHERE NAME=@name", parameters).Execute()[0]);
            int now1 = Convert.ToInt32(this.dbContext.S("select count(*) from person").QueryToDataTables()[0].Rows[0][0]);
            Assert.AreEqual(now1 + delCount, now);
            Assert.AreEqual(now1, currCount);
        }

        [TestMethod]
        public void Tran1()
        {
            var parameters = new DbParameter[] { new MySqlParameter("@name", MySqlDbType.VarChar) };
            parameters[0].Value = "test-test";
            int currCount1 = Convert.ToInt32(this.dbContext.S("select count(*) from person").QueryToDataTables()[0].Rows[0][0]);

            this.dbContext.BeginTranslation();
            var results = this.dbContext
               .S("insert into person(Name) values (@name)", parameters)
               .S("insert into person(Name) values (@name)", parameters)
               .S("insert into person(Name) values (@name)", parameters)
               .Execute();
            this.dbContext.Rollback();

            int currCount2 = Convert.ToInt32(this.dbContext.S("select count(*) from person").QueryToDataTables()[0].Rows[0][0]);
            Assert.AreEqual(currCount1, currCount2);

            this.dbContext.BeginTranslation();
            var results1 = this.dbContext
               .S("insert into person(Name) values (@name)", parameters)
               .S("insert into person(Name) values (@name)", parameters)
               .S("insert into person(Name) values (@name)", parameters)
               .Execute();
            this.dbContext.Commit();
            int currCount3 = Convert.ToInt32(this.dbContext.S("select count(*) from person").QueryToDataTables()[0].Rows[0][0]);
            Assert.AreEqual(currCount1 + 3, currCount3);
            this.dbContext.S("delete from person where Name='test-test'").Execute();
        }

        [TestMethod]
        public void Entity1()
        {
            var entitys = this.dbContext.S("select * from person").QueryToEntitys<Person>();
            Assert.AreEqual(entitys.Count(), 2);
        }

        [TestMethod]
        public void Entity2()
        {
            var entity = this.dbContext.S("select * from person where id=1").QueryFirstOrDefault<Person>();
            Assert.AreEqual(entity.ID, 1);
        }

        [TestMethod]
        public void Entity3()
        {
            var person = new Person()
            {
                Name = "tom"
            };
            var dbparameters = new MySqlParameter[1];
            dbparameters[0] = new MySqlParameter("@name", MySqlDbType.VarChar);
            dbparameters[0].Value = person.Name;
            int checkNotExist;
            checkNotExist = this.dbContext.S("select count(*) from person where Name=@name", dbparameters).QueryFirstOrDefault<int>();
            Assert.AreEqual(checkNotExist, 0);
            this.dbContext.InsertEntity(person);
            checkNotExist = this.dbContext.S("select count(*) from person where Name=@name", dbparameters).QueryFirstOrDefault<int>();
            Assert.AreEqual(checkNotExist, 1);
            var getResult = this.dbContext.S("select * from person where Name='tom'").QueryFirstOrDefault<Person>();
            Assert.IsNotNull(getResult);
            this.dbContext.Delete(getResult);
            checkNotExist = this.dbContext.S("select count(*) from person where Name=@name", dbparameters).QueryFirstOrDefault<int>();
            Assert.AreEqual(checkNotExist, 0);
        }

        [TestMethod]
        public void Dispose1()
        {
            this.dbContext.Dispose();
        }

        [TestMethod]
        public void T1()
        {
            var res = this.dbContext.T("select * from person where name=@0", "xiaoming").QueryFirstOrDefault<Person>();
            Assert.IsNotNull(res);
            Assert.AreEqual(res.Name, "xiaoming");
            var res2=this.dbContext.T("insert into person (name) values (@0)","test01").Execute();
            var res3=this.dbContext.T("delete from person where name=@0", "test01").Execute();
            Assert.AreEqual(res3[0], 1);
        }
    }

    public class Person
    {
        [Column("ID", true, true)]
        public int ID { get; set; }

        public string Name { get; set; }
    }
}