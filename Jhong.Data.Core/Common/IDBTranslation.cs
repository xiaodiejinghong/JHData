﻿namespace Jhong.Data.Core.Common
{
    using System.Data;

    internal interface IDBTranslation
    {
        void Commit();

        void Rollback();

        void BeginTranslation();

        void BeginTranslation(IsolationLevel level);
    }
}