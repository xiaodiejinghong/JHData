﻿namespace Jhong.Data.Core.Common
{
    internal interface IDBEntity
    {
        T[] QueryToEntitys<T>() where T : new();

        T QueryFirstOrDefault<T>() where T : new();

        int InsertEntity<T>(T model);

        int UpdateEntity<T>(T model);

        int DeleteEntity<T>(T model);

        int Delete<T>(string keyValue);
    }
}