﻿namespace Jhong.Data.Core
{
    using System;

    [AttributeUsage(AttributeTargets.Property, Inherited = false)]
    public class ColumnAttribute : Attribute
    {
        public string ColumnName { get; set; }

        public ColumnAttribute(string columnName)
            : this(columnName, false)
        {
        }

        public bool Key { get; set; }

        public bool AutoIncrement { get; set; }

        public ColumnAttribute(string columnName, bool key, bool autoincrement)
        {
            this.ColumnName = columnName;
            this.Key = key;
            this.AutoIncrement = autoincrement;
        }

        public ColumnAttribute(string columnName, bool key)
            : this(columnName, key, false)
        {
        }
    }

    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class TableAttribute : Attribute
    {
        public string TableName { get; set; }

        public TableAttribute(string tableName)
        {
            this.TableName = tableName;
        }
    }
}