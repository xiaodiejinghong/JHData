﻿namespace Jhong.Data.Test
{
    using Jhong.Data.Core;
    using System;
    using MySql.Data.MySqlClient;
    using System.Diagnostics;
    using System.Data;
    using Jhong.Data.MYSQL;

    internal class Program
    {
        private static void Main()
        {
            MYSQLTest(10000);
            //Console.WriteLine();
            //Console.WriteLine();
            //MYSQLTest(100000);

            //Console.ReadKey();

        }

        private static void MYSQLTest(int count)
        {
            var conn1 = new MySqlConnection("Server=127.0.0.1;Database=test;Uid=root;Pwd=root;Port=3306;");
            conn1.Open();
           /* var sw1 = new Stopwatch();
            sw1.Start();
            for (int i = 0; i < count; i++)
            {
                var cmd = new MySqlCommand("select * from person limit 1", conn1);
                var adt = new MySqlDataAdapter(cmd);
                var dt = new DataTable();
                adt.Fill(dt);
            }
            sw1.Stop();
            Console.WriteLine(string.Format("原生的{0}次查询，耗时:{1}毫秒", count, sw1.ElapsedMilliseconds));
            conn1.Close();

            var conn2 = new MySqlConnection("Server=127.0.0.1;Database=test;Uid=root;Pwd=root;Port=3306;");
            var dbContext = new MYSQLContext(conn2);
            var sw2 = new Stopwatch();
            sw2.Start();
            for (int i = 0; i < count; i++)
            {
                var dt2 = dbContext.S("select * from person limit 1").QueryToDataTables()[0];
            }
            sw2.Stop();
            dbContext.Dispose();
            Console.WriteLine(string.Format("ORM的{0}次查询，耗时:{1}毫秒", count, sw2.ElapsedMilliseconds));
            */
            var conn3 = new MySqlConnection("Server=127.0.0.1;Database=test;Uid=root;Pwd=root;Port=3306;");
            var dbContext3 = new MYSQLContext(conn3);
            var sw3 = new Stopwatch();
            sw3.Start();
            for (int i = 0; i < 10; i++)
            {
                var entity = dbContext3.S("select * from person limit 1").QueryFirstOrDefault<Person>();
            }
            sw3.Stop();
            dbContext3.Dispose();
            Console.WriteLine(string.Format("ORM的{0}次查询+压入Person实体，耗时:{1}毫秒", count, sw3.ElapsedMilliseconds));
        }



    }


}