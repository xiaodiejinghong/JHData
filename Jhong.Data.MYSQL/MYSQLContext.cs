﻿namespace Jhong.Data.MYSQL
{
    using Jhong.Data.Core.Common;
    using MySql.Data.MySqlClient;
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Linq;

    public class MYSQLContext : DBContext
    {
        public MYSQLContext(MySqlConnection connection)
            : base(connection)
        {
        }

        public MYSQLContext(string connectionString)
            : base(new MySqlConnection(connectionString))
        {
        }

        public override int[] Execute()
        {
            var count = this._execList.Count();
            var result = new int[count];
            for (int i = 0; i < count; i++)
            {
                var currExec = this._execList[i];
                result[i] = this.GetDBCommand(currExec.Item1, currExec.Item2).ExecuteNonQuery();
            }
            this.ClearExexList();
            return result;
        }

        public override IDbCommand GetDBCommand(string sqlStr, DbParameter[] parameters)
        {
            MySqlCommand cmd;
            if (null != this.Translation) cmd = new MySqlCommand(sqlStr, this.Connection as MySqlConnection, this.Translation as MySqlTransaction);
            else cmd = new MySqlCommand(sqlStr, this.Connection as MySqlConnection);
            if (null != parameters && parameters.Count() > 0) cmd.Parameters.AddRange(parameters);
            return cmd;
        }

        public override DataTable[] QueryToDataTables()
        {
            var count = this._execList.Count();
            var dts = new DataTable[count];
            for (int i = 0; i < count; i++)
            {
                var currExec = this._execList[i];
                var cmd = this.GetDBCommand(currExec.Item1, currExec.Item2) as MySqlCommand;
                dts[i] = this.FillData(cmd);
            }
            this.ClearExexList();
            return dts;
        }

        private DataTable FillData(MySqlCommand cmd)
        {
            var adt = new MySqlDataAdapter(cmd);
            var dt = new DataTable();
            adt.Fill(dt);
            adt.Dispose();
            return dt;
        }

        protected override DbParameter GetDBParameter(string parameterName, object value)
        {
            var parameter = new MySqlParameter();
            parameter.ParameterName = "@" + parameterName;
            switch (Type.GetTypeCode(value.GetType()))
            {
                case TypeCode.Boolean:
                    parameter.MySqlDbType = MySqlDbType.Bit; break;
                case TypeCode.Byte:
                    parameter.MySqlDbType = MySqlDbType.Byte; break;
                case TypeCode.Char:
                    parameter.MySqlDbType = MySqlDbType.VarChar; break;
                case TypeCode.DateTime:
                    parameter.MySqlDbType = MySqlDbType.DateTime; break;
                case TypeCode.Decimal:
                    parameter.MySqlDbType = MySqlDbType.Decimal; break;
                case TypeCode.Double:
                    parameter.MySqlDbType = MySqlDbType.Double; break;
                case TypeCode.Int16:
                    parameter.MySqlDbType = MySqlDbType.Int16; break;
                case TypeCode.Int32:
                    parameter.MySqlDbType = MySqlDbType.Int32; break;
                case TypeCode.Int64:
                    parameter.MySqlDbType = MySqlDbType.Int64; break;
                case TypeCode.Object:
                    parameter.MySqlDbType = MySqlDbType.Blob; break;
                case TypeCode.SByte:
                    parameter.MySqlDbType = MySqlDbType.UByte; break;
                case TypeCode.Single:
                    parameter.MySqlDbType = MySqlDbType.Float; break;
                case TypeCode.String:
                    parameter.MySqlDbType = MySqlDbType.VarChar; break;
                case TypeCode.UInt16:
                    parameter.MySqlDbType = MySqlDbType.UInt16; break;
                case TypeCode.UInt32:
                    parameter.MySqlDbType = MySqlDbType.UInt32; break;
                case TypeCode.UInt64:
                    parameter.MySqlDbType = MySqlDbType.UInt64; break;
                default:
                    throw new Exception("数据库不支持此类型的数据");
            }
            parameter.Value = value;
            return parameter;
        }
    }
}